/**
 * 
 * @author Jacek Kempski <kempski@limsatwork.de>
 * @created on 06.06.2016
 */
package com.waters.awm2.web.test;

import com.waters.awm2.web.WebConfig;

/**
 * @author Jacek Kempski <kempski@limsatwork.de>
 *
 */
public interface WebTestConfig extends WebConfig {

}
