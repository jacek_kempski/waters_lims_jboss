package com.waters.awm2.web;

import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.*;

import com.waters.awm2.ejb.EJBConfig;
import com.waters.awm2.ejb.backend.SimpleDatabaseConnectionBean;

public class ConnectionCheckerServlet extends HttpServlet
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        processRequest(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        processRequest(request, response);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String destinationPage = null;
        // perform action
        SimpleDatabaseConnectionBean l_bean = new SimpleDatabaseConnectionBean();
        l_bean.setG_username(EJBConfig.jk_username);
        l_bean.setG_password(EJBConfig.jk_password);
        l_bean.setG_connectionString(EJBConfig.jk_connectionString);
        String banner = null;
        try {
			banner = l_bean.getDatabaseResult();
		} catch (Exception e) {
			banner = e.getMessage();
		}
        request.setAttribute("banner", banner);
		destinationPage = "/banner.jsp";
        // Redirect to destination page.
        RequestDispatcher dispatcher =  getServletContext().getRequestDispatcher(destinationPage);
        dispatcher.forward(request, response);
    }
}
