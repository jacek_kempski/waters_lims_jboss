/**
 * 
 * @author Jacek Kempski <kempski@limsatwork.de>
 * @created on 08.06.2016
 */


package com.waters.awm2.ejb.backend;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.waters.awm2.ejb.exceptions.InsufficientDatabaseConfigurationException;

import oracle.jdbc.pool.OracleDataSource;

/**
 * @author Jacek Kempski <kempski@limsatwork.de>
 * @created on 08.06.2016
 */


public class SimpleDatabaseConnectionBean {

	/*--------------------
	* PUBLIC METHODS
	--------------------*/
	public String getDatabaseResult() throws InsufficientDatabaseConfigurationException, SQLException{
		String r_banner = null;
		Connection conToBeUsed = getConnection();
		PreparedStatement l_pStatement = conToBeUsed.prepareStatement(g_queryStatement);
		ResultSet l_rs = l_pStatement.executeQuery();
		l_rs.next();
		r_banner = l_rs.getString(1);
		l_rs.close();
		this.g_con.close();
		return r_banner;
	}
	
	public Connection getConnection() throws InsufficientDatabaseConfigurationException, SQLException{
		if( null == g_con ){
			this.init();
		}
		OracleDataSource ods = new OracleDataSource();
		ods.setURL(this.g_connectionString);
		ods.setUser(this.g_username);
		ods.setPassword(this.g_password);
		this.g_con = ods.getConnection();
		return g_con;
	}
	/**
	 * @return the g_username
	 */
	public String getG_username() {
		return g_username;
	}
	/**
	 * @param g_username the g_username to set
	 */
	public void setG_username(String g_username) {
		this.g_username = g_username;
	}
	/**
	 * @return the g_password
	 */
	public String getG_password() {
		return g_password;
	}
	/**
	 * @param g_password the g_password to set
	 */
	public void setG_password(String g_password) {
		this.g_password = g_password;
	}
	/**
	 * @return the g_connectionString
	 */
	public String getG_connectionString() {
		return g_connectionString;
	}
	/**
	 * @param g_connectionString the g_connectionString to set
	 */
	public void setG_connectionString(String g_connectionString) {
		this.g_connectionString = g_connectionString;
	}

	/*--------------------
	* PRIVATE METHODS
	--------------------*/
	
	private void init() throws InsufficientDatabaseConfigurationException{
		if( null == g_username ){
			throw new InsufficientDatabaseConfigurationException( "Username not specified." );
		}else if ( null == g_password ) {
			throw new InsufficientDatabaseConfigurationException( "Password not specified." );
		}else if ( null == g_connectionString ){
			throw new InsufficientDatabaseConfigurationException( "Connection string not specified.");
		}
		
	}
	/*--------------------
	* PRIVATE MEMBERS
	--------------------*/
	private Connection g_con = null;
	private String g_username = null;
	private String g_password = null;
	private String g_connectionString = null;
	private String g_queryStatement = "SELECT banner FROM v$version";
	
}
