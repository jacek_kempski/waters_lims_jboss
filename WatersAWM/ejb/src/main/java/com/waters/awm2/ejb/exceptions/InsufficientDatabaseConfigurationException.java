/**
 * 
 * @author Jacek Kempski <kempski@limsatwork.de>
 * @created on 08.06.2016
 */


package com.waters.awm2.ejb.exceptions;

/**
 * @author Jacek Kempski <kempski@limsatwork.de>
 * @created on 08.06.2016
 */

public class InsufficientDatabaseConfigurationException extends Exception {

	
	/*--------------------
	* PUBLIC METHODS
	--------------------*/
	/**
	 * 
	 */
	public InsufficientDatabaseConfigurationException() {
		super(g_baseMessage);
	}

	/**
	 * @param message
	 */
	public InsufficientDatabaseConfigurationException(String message) {
		super(new StringBuffer(g_baseMessage).append(message).toString());
	}

	/**
	 * @param cause
	 */
	public InsufficientDatabaseConfigurationException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public InsufficientDatabaseConfigurationException(String message, Throwable cause) {
		super(new StringBuffer(g_baseMessage).append(message).toString(), cause);
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public InsufficientDatabaseConfigurationException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(new StringBuffer(g_baseMessage).append(message).toString(), cause, enableSuppression, writableStackTrace);

	}
	/*--------------------
	* PRIVATE MEMBERS
	--------------------*/
	private static final String g_baseMessage= "Insufficient Database Info. ";
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
