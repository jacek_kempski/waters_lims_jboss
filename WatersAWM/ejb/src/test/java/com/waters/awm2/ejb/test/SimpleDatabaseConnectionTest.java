/**
 * 
 * @author Jacek Kempski <kempski@limsatwork.de>
 * @created on 09.06.2016
 */


package com.waters.awm2.ejb.test;

import java.sql.Connection;
import java.sql.SQLException;

import org.junit.Assert;
import org.junit.Test;

import com.waters.awm2.ejb.EJBConfig;
import com.waters.awm2.ejb.backend.SimpleDatabaseConnectionBean;
import com.waters.awm2.ejb.exceptions.InsufficientDatabaseConfigurationException;

/**
 * @author Jacek Kempski <kempski@limsatwork.de>
 * @created on 09.06.2016
 */

public class SimpleDatabaseConnectionTest {

	@Test
	public void testConnectionNotNull(){
		SimpleDatabaseConnectionBean l_bean = new SimpleDatabaseConnectionBean();
		l_bean.setG_username(EJBConfig.jk_username);
		l_bean.setG_password(EJBConfig.jk_password);
		l_bean.setG_connectionString(EJBConfig.jk_connectionString);
		try{
			Connection l_con = l_bean.getConnection();
			Assert.assertNotNull(l_con);
			l_con.close();
		}catch(InsufficientDatabaseConfigurationException idce){
			
		}catch(SQLException sqe){
			
		}
	}

}
