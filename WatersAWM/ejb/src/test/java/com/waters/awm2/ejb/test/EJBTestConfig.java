/**
 * 
 * @author Jacek Kempski <kempski@limsatwork.de>
 * @created on 06.06.2016
 */
package com.waters.awm2.ejb.test;

import com.waters.awm2.ejb.EJBConfig;

/**
 * @author Jacek Kempski <kempski@limsatwork.de>
 *
 */
public interface EJBTestConfig extends EJBConfig {

}
